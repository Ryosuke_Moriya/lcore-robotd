#!/usr/bin/env python
import sys
import rospy
import baxter_interface


class Mover:
    def __init__(self):
        print("Mover init")

    def try_float(self,x):
        try:
            return float(x)
        except ValueError:
            return None

    def map_file(self, right, cdn_list, IKservice, loops=1):

        rate = rospy.Rate(1000)


        print("Playing back: %s" % ("record.rec",))
        with open("./lcore-robotd/joint.txt", 'r') as f:
            lines = f.readlines()
        keys = lines[0].rstrip().split(',')

        l = 0

        while loops < 1 or l < loops:
            i = 0
            l += 1
            t = 0.0
            print("Moving to start position...")

            rcmd_start = IKservice.solve(cdn_list[0])
            right.move_to_joint_positions(rcmd_start, 0.5)
            start_time = rospy.get_time()
            for values in cdn_list[1:]:
                i += 1
                t += 0.02337099499
                loopstr = str(loops) if loops > 0 else "forever"
                sys.stdout.write("\r Record %d of %d, loop %d of %s" %
                                (i, len(cdn_list) - 1, l, loopstr))
                sys.stdout.flush()
                rcmd = IKservice.solve(cdn_list[i])
                values = [t]
                values.extend(rcmd.values())
                while (rospy.get_time() - start_time) < values[0]:
                    if rospy.is_shutdown():
                        print("\n Aborting - ROS shutdown")
                        return False
                    if len(cdn_list):
                        right.set_joint_positions(rcmd)
                    rate.sleep()
            return True
