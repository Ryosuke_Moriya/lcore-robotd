#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sstream>
#include <unistd.h>
int main()
{
 struct sockaddr_in server;
 int sock;
 char buf[32];
 int n;
 char buffer[1024];
 char sdata[1024];

 char command[] = "gotoready\n";
 int status;

 sock = socket(AF_INET, SOCK_STREAM, 0);

 server.sin_family = AF_INET;
 server.sin_port = htons(12345);
 server.sin_addr.s_addr = inet_addr("127.0.0.1");

 connect(sock, (struct sockaddr *)&server, sizeof(server));
 memset(buf, 0, sizeof(buf));
 std::stringstream sts;
 // fgets(buffer,sizeof buffer,stdin);
 // strcat(buffer,"\n");
 /*sts << buffer;*/
 /*sts >> sdata;*/
 // const char* sdataconst= buffer;
 if ((status = send(sock, command, strlen(command), MSG_NOSIGNAL)) >= 0) {
   fprintf(stderr, "sent: %d, %s\n", status, command);
 }
 /*send(sock, sbuf, sizeof(sbuf), 0);*/


 n = read(sock, buf, sizeof(buf));

 printf("%d, %s\n", n, buf);


 char next_command[] = "move_arm_traj -t100 0.701382 -0.434973 -0.081451 0.701805 -0.434890 -0.081820 0.702211 -0.434812 -0.082103 0.702584 -0.434745 -0.082218 0.702919 -0.434691 -0.082146 0.703221 -0.434646 -0.081933 0.703499 -0.434610 -0.081616 0.703750 -0.434581 -0.081180 0.703976 -0.434558 -0.080600 0.704176 -0.434542 -0.079878 0.704352 -0.434533 -0.079042 0.704505 -0.434531 -0.078116 0.704636 -0.434536 -0.077094 0.704744 -0.434547 -0.075961 0.704830 -0.434565 -0.074708 0.704439 -0.434707 -0.070620 0.704441 -0.434742 -0.068765 0.704416 -0.434781 -0.066737 0.704362 -0.434823 -0.064530 0.704281 -0.434866 -0.062140\n";
 if ((status = send(sock, next_command, strlen(next_command), MSG_NOSIGNAL)) >= 0) {
   fprintf(stderr, "sent: %d, %s\n", status, next_command);
 }
 n = read(sock, buf, sizeof(buf));
 printf("%d, %s\n", n, buf);

 close(sock);

 return 0;
}
